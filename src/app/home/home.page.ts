import { Component } from '@angular/core';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{
  constructor(private todoService: TodoService){}

  ionViewWillEnter(){
    this.liste = [];
    this.getListe();
  }
  liste = [];

  getListe(){
    this.todoService.getListe()
      .subscribe(data=>{
        for(let i = 0; i < Object.keys(data).length; i++){
          this.liste.push({'key':Object.keys(data)[i],'values':Object.values(data)[i]})
        }
      });
  }


}
