import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  indice:number;
  tache;

  constructor(private route: ActivatedRoute, private todoService: TodoService, private router: Router) {
    this.route.params
    .subscribe( params => this.indice = params.indice)
   }

  ngOnInit() {
    this.getElement(this.indice);
  }

  getElement(indice){
    this.todoService.getElement(indice)
      .subscribe(data=>{
        this.tache = data;
      })
  }
  edit(form){
    this.todoService.edit(form.form.value, this.indice)
      .subscribe(data=>{
        this.router.navigate(['/home']);
      })
  }

  delete(){
    this.todoService.delete( this.indice)
      .subscribe(data=>{
        this.router.navigate(['/home']);
      })
  }

}
