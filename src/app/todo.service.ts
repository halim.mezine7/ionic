import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Tache } from './tache';


@Injectable({
  providedIn: 'root'
})
export class TodoService {

  liste = [{
    "titre":"Bonjour",
    "description":'Aujourdhui on s amuse comme des fous'
  },{
    "titre":"Au revoir ",
    "description":'C est la folie chez Aston'
  }];
  constructor(private http: HttpClient) { }

  //Retourne toutes les taches
  getListe():Observable<Tache[]>{
    return this.http.get<Tache[]>('https://todo-9b2f8.firebaseio.com/tache.json')
      .pipe(
        map(data=>data)
      );
  }

  //Recupere un element specifique
  getElement(key):Observable<Tache[]>{
    return this.http.get<Tache[]>('https://todo-9b2f8.firebaseio.com/tache/'+key+'.json')
      .pipe(
        map(data=>data)
      );
  }

  //Ajoute une tache
  add(form: Tache):Observable<Tache>{
    return this.http.post<Tache>('https://todo-9b2f8.firebaseio.com/tache.json',form)
    .pipe(
      map(data=>data)
    )
  }

  //Modifie une tache
  edit(form, key):Observable<Tache>{
    return this.http.put<Tache>('https://todo-9b2f8.firebaseio.com/tache/'+key+'.json', form)
      .pipe(
        map(data=>data)
      )
  }

  //Delete une tache
  delete(key):Observable<void>{
    return this.http.delete<void>('https://todo-9b2f8.firebaseio.com/tache/'+key+'.json')
      .pipe(
        map(data=>data)
      )
  }

}
