import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  constructor(private todoService: TodoService, private router:Router) { }

  ngOnInit() {
  }

  add(form){
    this.todoService.add(form.form.value)
      .subscribe(
        data=>{
          console.log('Tache ajoutee :'+data);
          this.router.navigate(['/home']);
        }
      )
  }


}
